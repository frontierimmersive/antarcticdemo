﻿using UnityEngine;
using System.Collections;

public enum TigerState {
	Start,
	Walking,
	Roar,
	Sitting,
	Eat
}

public class TigerController : MonoBehaviour {


	public Animator moveAnimator;
	public Animator actionAnimator;
	
	public TigerState state = TigerState.Start;

	public AudioSource tigerRoar;
	
	public bool isBaby = false;
	
	private int _lastAnimState;
	
	void Start () {
		Controller.OnResetToStart += HandleOnResetToStart;
	}
	
	void HandleOnResetToStart (){
		moveAnimator.Play("TigerMove");
		actionAnimator.Play(isBaby ? "Walk Fast" : "Walk");
		state = TigerState.Walking;
	}
	
	void Update () {
		if (state == TigerState.Walking){
			// use new unity anim events when i can here instead
			if (_lastAnimState == Animator.StringToHash("Base.TigerMove") && moveAnimator.GetCurrentAnimatorStateInfo(0).IsName("Base.TigerWaterPosition")){
				if (!isBaby){
					DoRoar();
				} else {
					DoEat();
				}
			}
			_lastAnimState = moveAnimator.GetCurrentAnimatorStateInfo(0).nameHash;
		}
	}
	private void DoEat(){
		state = TigerState.Eat;
		actionAnimator.CrossFade("Eat", 0.2f);
	}
	
	private void DoRoar(){
		state = TigerState.Roar;
		actionAnimator.CrossFade("Roar", 0.2f);
		
		if (tigerRoar != null){
			tigerRoar.PlayDelayed(0.65f);
		}
	}

	public void SitDown(){
		state = TigerState.Sitting;
		actionAnimator.Play(isBaby ? "Lick" : "Sitting", 0, Random.Range(0f, 1f));
	}

	
}
