﻿using UnityEngine;
using System.Collections;

public class ElephantController : MonoBehaviour {
	
	private Animator animator;
	public AudioSource TrumpetSound;
	
	void Start () {
		animator = GetComponent<Animator>();
		animator.Play("Idle", 0, Random.Range(0f, 1f));
	}
	
	public void Trumpet(){
		animator.Play("Start Trumpet");
		TrumpetSound.Play();
	}
	
	public void LieDown(){
		animator.Play("Lie Down");
	}
}
